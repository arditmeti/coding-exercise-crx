# Rainy hills coding exercise

### Algorithm
The idea is to check the highest hill on the left and on the right of the current hill that you want to calculate the water above it.
So I preprocessed and stored the highest hill on the left and on the right for each of the hills in 2 int arrays.

Then with another loop I calculate how much water is above for each index: 
```
Math.min(highestFromLeft[i], highestFromRight[i]) - input[i];
```

Time complexity: O(n) where n is the length of the input array(hills).

Space complexity: O(n) since I use 2 extra arrays with n length.

### Run the project

Build the maven project ```mvn clean install``` and run the spring boot application.

### URLs


1. http://localhost:8081/api/v1/rainy-hills
   
2. http://localhost:8081/rainy-hills-ui 
   
Both these urls expect a query parameter hills which is a concatenated string. Ex:
http://localhost:8081/api/v1/rainy-hills?hills=1,2,3,2,3,7,6,7 or http://localhost:8081/rainy-hills-ui?hills=1,2,3,2,3,7,6,7
### Run via Docker
Run this image from dockerhub
``` 
sudo docker run -p 8081:8081 arditmeti/crxcodeexercise
```


### API Docs

http://localhost:8081/api-docs

http://localhost:8081/api-ui.html (Swagger UI)