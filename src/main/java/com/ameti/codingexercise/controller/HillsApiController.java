package com.ameti.codingexercise.controller;

import com.ameti.codingexercise.api.RainyHillsApi;
import com.ameti.codingexercise.models.HillsResponse;
import com.ameti.codingexercise.service.CalculateWaterOnHillsService;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Locale;

@RestController
@RequestMapping("/api/v1")
public class HillsApiController implements RainyHillsApi {

    final CalculateWaterOnHillsService calculateWaterOnHillsService;
    final MessageSource messageSource;

    public HillsApiController(CalculateWaterOnHillsService calculateWaterOnHillsService, MessageSource messageSource) {
        this.calculateWaterOnHillsService = calculateWaterOnHillsService;
        this.messageSource = messageSource;
    }

    @Override
    public ResponseEntity<HillsResponse> getWaterOnRainyHills(String hills) {
        if(!calculateWaterOnHillsService.validInput(hills)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, messageSource.getMessage("wrongInput", null, Locale.getDefault()));
        }

        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills(hills);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
}
