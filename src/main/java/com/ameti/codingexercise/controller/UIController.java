package com.ameti.codingexercise.controller;

import com.ameti.codingexercise.models.HillsResponse;
import com.ameti.codingexercise.service.CalculateWaterOnHillsService;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.util.Locale;

@Controller
@RequestMapping
public class UIController {

    final CalculateWaterOnHillsService calculateWaterOnHillsService;
    final MessageSource messageSource;

    public UIController(CalculateWaterOnHillsService calculateWaterOnHillsService, MessageSource messageSource) {
        this.calculateWaterOnHillsService = calculateWaterOnHillsService;
        this.messageSource = messageSource;
    }

    @GetMapping("/rainy-hills-ui")
    public String app(@RequestParam(name="hills") String hills, Model model) {
        if(!calculateWaterOnHillsService.validInput(hills)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, messageSource.getMessage("wrongInput", null, Locale.getDefault()));
        }

        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills(hills);
        model.addAttribute("hillsInput", res.getHills());
        model.addAttribute("waterOnHills", res.getWaterOnHills());
        model.addAttribute("totalWater", res.getTotalWaterVolume());
        return "app";
    }
}
