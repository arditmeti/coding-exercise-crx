package com.ameti.codingexercise.service;

import com.ameti.codingexercise.models.HillsResponse;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class CalculateWaterOnHillsService {

    public HillsResponse calculateWaterOnRainyHills(String hills) {
        int[] input = Arrays.stream(hills.split(",")).mapToInt(Integer::parseInt).toArray();

        int[] highestFromLeft = new int[input.length];
        int[] highestFromRight = new int[input.length];
        int[] waterOnHills = new int[input.length];

        int totalWater = 0;
        highestFromLeft[0] = input[0];
        highestFromRight[input.length-1] = input[input.length-1];

        for(int i = 1; i < input.length; i++) {
            highestFromLeft[i] = Math.max(highestFromLeft[i-1], input[i]);
        }

        for(int i = input.length - 2; i >= 0; i--) {
            highestFromRight[i] = Math.max(input[i], highestFromRight[i+1]);
        }

        for(int i = 0; i < input.length; i++) {
            waterOnHills[i] = Math.min(highestFromLeft[i], highestFromRight[i]) - input[i];
            totalWater += waterOnHills[i];
        }

        HillsResponse res = new HillsResponse();
        res.setHills(Arrays.stream(input).boxed().collect(Collectors.toList()));
        res.setWaterOnHills(Arrays.stream(waterOnHills).boxed().collect(Collectors.toList()));
        res.setTotalWaterVolume(totalWater);
        return res;
    }

    public boolean validInput(String hills) {
        return hills.matches("\\d+(?:,\\d+)+") || hills.matches("\\d");
    }
}
