package com.ameti.codingexercise.service;

import com.ameti.codingexercise.models.HillsResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class CalculateWaterOnHillsServiceTests {

    CalculateWaterOnHillsService calculateWaterOnHillsService = new CalculateWaterOnHillsService();

    @Test
    public void testEmptyInput() {
        assertFalse(calculateWaterOnHillsService.validInput(""));
    }

    @Test
    public void testSingleHillInput() {
        assertTrue(calculateWaterOnHillsService.validInput("1"));
    }

    @Test
    public void testCharInInput() {
        assertFalse(calculateWaterOnHillsService.validInput("1,d,1,3"));
    }

    @Test
    public void testSpacesInInput() {
        assertFalse(calculateWaterOnHillsService.validInput("1,  1,1 ,3"));
    }

    @Test
    public void testValidInput() {
        assertTrue(calculateWaterOnHillsService.validInput("1,2,3,4"));
    }

    @Test
    public void calculateWaterOnRainyHills_singleHill() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("1");
        assertEquals(0, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Collections.singletonList(0), res.getWaterOnHills());
    }

    @Test
    public void calculateWaterOnRainyHills_increasingHills() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("1,2,3,4");
        assertEquals(0, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Arrays.asList(0,0,0,0), res.getWaterOnHills());
    }

    @Test
    public void calculateWaterOnRainyHills_decreasingHills() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("4,3,2,1");
        assertEquals(0, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Arrays.asList(0,0,0,0), res.getWaterOnHills());
    }

    @Test
    public void calculateWaterOnRainyHills_noHills() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("0,0,0,0");
        assertEquals(0, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Arrays.asList(0,0,0,0), res.getWaterOnHills());
    }

    @Test
    public void calculateWaterOnRainyHills_sameHeightHills() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("4,4,4,4");
        assertEquals(0, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Arrays.asList(0,0,0,0), res.getWaterOnHills());
    }

    @Test
    public void calculateWaterOnRainyHills_randomHills() {
        HillsResponse res = calculateWaterOnHillsService.calculateWaterOnRainyHills("4,3,4,2,2,1,7");
        assertEquals(8, res.getTotalWaterVolume());
        Assertions.assertIterableEquals(Arrays.asList(0,1,0,2,2,3,0), res.getWaterOnHills());
    }
}
