package com.ameti.codingexercise.controller;

import com.ameti.codingexercise.models.HillsResponse;
import com.ameti.codingexercise.service.CalculateWaterOnHillsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UIController.class)
public class UIControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CalculateWaterOnHillsService calculateWaterOnHillsService;

    @Test
    public void testGetBadRequest() throws Exception {
        when(calculateWaterOnHillsService.validInput(anyString())).thenReturn(false);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/rainy-hills-ui?hills=1,s,3,2,3,7,6,7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetOKRequest() throws Exception {
        when(calculateWaterOnHillsService.validInput(anyString())).thenReturn(true);
        when(calculateWaterOnHillsService.calculateWaterOnRainyHills(anyString())).thenReturn(new HillsResponse());

        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/rainy-hills-ui?hills=1,1,3,2,3,7,6,7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
