package com.ameti.codingexercise.controller;

import com.ameti.codingexercise.models.HillsResponse;
import com.ameti.codingexercise.service.CalculateWaterOnHillsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(HillsApiController.class)
public class HillsApiControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CalculateWaterOnHillsService calculateWaterOnHillsService;

    @Test
    public void testGetBadRequest() throws Exception {
        when(calculateWaterOnHillsService.validInput(anyString())).thenReturn(false);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rainy-hills?hills=1,s,3,2,3,7,6,7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetOKRequest() throws Exception {
        when(calculateWaterOnHillsService.validInput(anyString())).thenReturn(true);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rainy-hills?hills=1,1,3,2,3,7,6,7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetOKJSONResponse() throws Exception {
        when(calculateWaterOnHillsService.validInput(anyString())).thenReturn(true);
        HillsResponse res = new HillsResponse();
        res.setHills(List.of(4,2,3,4));
        res.setWaterOnHills(List.of(0,2,1,0));
        res.setTotalWaterVolume(3);
        when(calculateWaterOnHillsService.calculateWaterOnRainyHills(anyString())).thenReturn(res);

        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rainy-hills?hills=1,1,3,2,3,7,6,7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"hills\":[4,2,3,4],\"waterOnHills\":[0,2,1,0],\"totalWaterVolume\":3}"));
        ;
    }
}
